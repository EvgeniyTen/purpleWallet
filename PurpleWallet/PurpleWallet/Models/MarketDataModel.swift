//
//  MarketDataModel.swift
//  PurpleWallet
//
//  Created by user on 09.12.2022.
//

//JSON Data:
/*
 URL: https://api.coingecko.com/api/v3/global
 JSON Response:
 {
   "data": {
     "active_cryptocurrencies": 12988,
     "upcoming_icos": 0,
     "ongoing_icos": 49,
     "ended_icos": 3376,
     "markets": 612,
     "total_market_cap": {
       "btc": 51927078.44711764,
       "eth": 701809575.4945697,
       "ltc": 11678145903.22252,
       "bch": 8090182768.44328,
       "bnb": 3093438945.780063,
       "eos": 858118143907.5654,
       "xrp": 2294665590787.405,
       "xlm": 10460071049546.918,
       "link": 130279671723.67986,
       "dot": 168753422278.49396,
       "yfi": 128380655.59935474,
       "usd": 890466359103.4663,
       "aed": 3270682936987.033,
       "ars": 151254526710678.66,
       "aud": 1308762040825.9595,
       "bdt": 92017100785761.25,
       "bhd": 335678212924.8746,
       "bmd": 890466359103.4663,
       "brl": 4668002747692.191,
       "cad": 1213353913246.1765,
       "chf": 831310007469.1438,
       "clp": 767582001547188.5,
       "cny": 6195686833370.09,
       "czk": 20505675345829.09,
       "dkk": 6278689874101.208,
       "eur": 844228002940.6615,
       "gbp": 725449585766.205,
       "hkd": 6931171975003.42,
       "huf": 353539925352383.8,
       "idr": 13876532348899812,
       "ils": 3044549005092.706,
       "inr": 73392461714830.36,
       "jpy": 121507518471832.56,
       "krw": 1160731722722518.2,
       "kwd": 273109594202.4696,
       "lkr": 327201714054220.44,
       "mmk": 1869728190956961.8,
       "mxn": 17614755830273.4,
       "myr": 3921168612312.113,
       "ngn": 396230815810269.2,
       "nok": 8880114323980.562,
       "nzd": 1386281589717.7146,
       "php": 49249909827429.086,
       "pkr": 200043267572593.6,
       "pln": 3956629654130.694,
       "rub": 55667506220286.04,
       "sar": 3348862321450.8813,
       "sek": 9181891592147.99,
       "sgd": 1203376237692.4253,
       "thb": 30899182660890.28,
       "try": 16602300032304.578,
       "twd": 27307219461538.69,
       "uah": 32713967348204.863,
       "vef": 89162396537.0302,
       "vnd": 21193099346662480,
       "zar": 15454204836651.656,
       "xdr": 671787411567.556,
       "xag": 37799700331.48156,
       "xau": 495063677.0071633,
       "bits": 51927078447117.64,
       "sats": 5192707844711764
     },
     "total_volume": {
       "btc": 2626034.359805123,
       "eth": 35491618.52357759,
       "ltc": 590582280.339749,
       "bch": 409133318.53767437,
       "bnb": 156440092.61662397,
       "eos": 43396389669.185165,
       "xrp": 116044862639.57587,
       "xlm": 528982311417.4789,
       "link": 6588448735.449808,
       "dot": 8534127058.37732,
       "yfi": 6492412.491134998,
       "usd": 45032289995.626915,
       "aed": 165403601153.93774,
       "ars": 7649180275428.177,
       "aud": 66186163188.782616,
       "bdt": 4653450099241.486,
       "bhd": 16975777327.361483,
       "bmd": 45032289995.626915,
       "brl": 236068270615.07544,
       "cad": 61361223509.4911,
       "chf": 42040659874.347336,
       "clp": 38817833976230.43,
       "cny": 313325667331.5726,
       "czk": 1037004384600.5167,
       "dkk": 317523262178.9353,
       "eur": 42693943305.31408,
       "gbp": 36687131175.08719,
       "hkd": 350520312414.9119,
       "huf": 17879072331862.184,
       "idr": 701758154568022.2,
       "ils": 153967651109.5482,
       "inr": 3711572689576.6577,
       "jpy": 6144827092605.275,
       "krw": 58700036234261.13,
       "kwd": 13811583470.818762,
       "lkr": 16547107393468.396,
       "mmk": 94555107273164.86,
       "mxn": 890806019387.0427,
       "myr": 198299688995.74307,
       "ngn": 20038018079354.098,
       "nok": 449081404753.3809,
       "nzd": 70106449194.35207,
       "php": 2490645714948.974,
       "pkr": 10116503947517.582,
       "pln": 200093009880.2391,
       "rub": 2815193699141.203,
       "sar": 169357256086.3938,
       "sek": 464342757768.31793,
       "sgd": 60856636700.09026,
       "thb": 1562620462848.2542,
       "try": 839604530823.4661,
       "twd": 1380969211463.8928,
       "uah": 1654396990375.9802,
       "vef": 4509083197.262129,
       "vnd": 1071768501895919.8,
       "zar": 781543543718.5944,
       "xdr": 33973350283.080887,
       "xag": 1911590538.6800663,
       "xau": 25036151.94596875,
       "bits": 2626034359805.123,
       "sats": 262603435980512.28
     },
     "market_cap_percentage": {
       "btc": 37.0310872456918,
       "eth": 17.173214055564923,
       "usdt": 7.382209542941773,
       "bnb": 5.277933843603487,
       "usdc": 4.8090512678174795,
       "busd": 2.4814211318771497,
       "xrp": 2.1962006597695627,
       "doge": 1.4898537251351998,
       "ada": 1.2246691789126332,
       "matic": 0.9220209839498635
     },
     "market_cap_change_percentage_24h_usd": 0.1148137369681113,
     "updated_at": 1670615476
   }
 }
 */

import Foundation

struct GlobalData: Codable {
    let data: MarketDataModel?
}

// MARK: - MarketDataModel
struct MarketDataModel: Codable {
    let totalMarketCap, totalVolume, marketCapPercentage: [String: Double]
    let marketCapChangePercentage24HUsd: Double

    enum CodingKeys: String, CodingKey {
        case totalMarketCap = "total_market_cap"
        case totalVolume = "total_volume"
        case marketCapPercentage = "market_cap_percentage"
        case marketCapChangePercentage24HUsd = "market_cap_change_percentage_24h_usd"
    }
    
    
    var marketCap: String {
        if let item = totalMarketCap.first(where: { $0.key == "usd"}) {
            return "$" + item.value.formattedWithAbbreviations()
        }
        return ""
    }
    
    var volume: String {
        if let item = totalVolume.first(where: { $0.key == "usd"}) {
            return "$" +  item.value.formattedWithAbbreviations()
        }
        return ""
    }
    
    var btcDominance: String {
        if let item = marketCapPercentage.first(where: { $0.key == "btc"}) {
            return item.value.asPercentString()
        }
        return ""
    }
}


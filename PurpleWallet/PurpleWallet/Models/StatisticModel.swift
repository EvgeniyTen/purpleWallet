//
//  StatisticModel.swift
//  PurpleWallet
//
//  Created by user on 27.11.2022.
//

import Foundation


struct StatisticModel: Identifiable {
    let id = UUID().uuidString
    let title: String
    let value: String
    let percengaheChanged: Double?
    
    init(title: String, value: String, percengaheChanged: Double? = nil) {
        self.title = title
        self.value = value
        self.percengaheChanged = percengaheChanged
    }
}


//
//  PurpleWalletApp.swift
//  PurpleWallet
//
//  Created by user on 25.11.2022.
//

import SwiftUI

@main
struct PurpleWalletApp: App {
    
    @StateObject private var viewmodel = HomeViewModel()
    
    init() {
        UINavigationBar.appearance().largeTitleTextAttributes = [.foregroundColor: UIColor(.theme.accent)]
        UINavigationBar.appearance().titleTextAttributes = [.foregroundColor: UIColor(.theme.accent)]

    }
    
    var body: some Scene {
        WindowGroup {
            NavigationView {
                
                HomeView()
                    .toolbar(.hidden, for: .navigationBar)
            }
            .environmentObject(viewmodel)
        }
    }
}

//
//  MarketDataService.swift
//  PurpleWallet
//
//  Created by user on 09.12.2022.
//

import Combine
import Foundation


class MarketDataService {
    @Published var marketData: MarketDataModel? = nil
    var marketDataSubscription: AnyCancellable?
    
    init() {
        getData()
    }
    
    private func getData() {
        guard let url = URL(string: "https://api.coingecko.com/api/v3/global") else {return}
        marketDataSubscription = NetworkManager.download(url: url)
            .decode(type: GlobalData.self, decoder: JSONDecoder())
            .sink(receiveCompletion: NetworkManager.handleCompletion, receiveValue: { [weak self] (returnedData) in
                guard let self = self else {return}
                self.marketData = returnedData.data
                self.marketDataSubscription?.cancel()
            })
    }
}

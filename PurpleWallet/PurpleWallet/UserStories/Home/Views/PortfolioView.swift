//
//  PortfolioView.swift
//  PurpleWallet
//
//  Created by user on 10.12.2022.
//

import SwiftUI

struct PortfolioView: View {
    
    @Environment(\.dismiss) private var dismiss
    @EnvironmentObject private var viewModel: HomeViewModel
    @State private var selectedCoin: CoinModel? = nil
    @State private var quantityText = ""
    
    var body: some View {
        NavigationView {
            ScrollView {
                VStack(alignment: .leading, spacing: 10) {
                    SearchBarView(searchText: $viewModel.searchText)
                    coinLogoList
                    if selectedCoin != nil {
                        portfolioInputView
                        .animation(.none, value: selectedCoin?.id)
                        .padding()
                    }
                }
            }
            .navigationTitle("Edit portfolio")
            .toolbar(content: {
                ToolbarItem(placement: .navigationBarLeading) {
                    XmarkButton(dismiss: dismiss)
                }
                ToolbarItem(placement: .navigationBarTrailing) {
                    saveButton
                }
            })
        }
        
    }
}

struct PortfolioView_Previews: PreviewProvider {
    static var previews: some View {
        PortfolioView()
            .environmentObject(mock.homeViewModel)
    }
}


// MARK: - Components

extension PortfolioView {
    private var coinLogoList: some View {
        ScrollView(.horizontal, showsIndicators: false) {
            LazyHStack(spacing: 10) {
                ForEach(viewModel.allCoins) { coin in
                    CoinLogoView(coin: coin)
                    
                        .onTapGesture {
                            withAnimation(.easeIn) {
                                selectedCoin = coin
                            }
                        }
                        .background(
                            RoundedRectangle(cornerRadius: 10)
                                .stroke(selectedCoin?.id == coin.id ? Color.theme.green  : Color.theme.secondaryText, lineWidth: selectedCoin?.id == coin.id ? 1.0 : 0.1)
                                .shadow(color: .black, radius: 10)
                        )
                }
            }
            .padding(.vertical, 4)
            .padding(.leading)
        }
    }
    
    private var portfolioInputView: some View {
        VStack(spacing: 20) {
            HStack {
                Text("Current price is of \(selectedCoin?.symbol.uppercased() ?? "") :")
                Spacer()
                Text(selectedCoin?.currentPrice.asCurrencyWith6Decimals() ?? "")
            }
            Divider()
            HStack {
                Text("Amount holding:")
                Spacer()
                TextField("For example: 1.4", text: $quantityText)
                    .multilineTextAlignment(.trailing)
                    .keyboardType(.decimalPad)
            }
            Divider()
            HStack {
                Text("Current value: ")
                Spacer()
                Text(getCurrentValue().asCurrencyWith6Decimals())

            }
        }
    }
    
    private var saveButton: some View {
        HStack(spacing: 10) {
            Button("Save") {
                saveButtonPressed()
            }
        }
        .opacity(
            selectedCoin != nil && selectedCoin?.currentHoldings != Double(quantityText) ? 1.0 : 0.0
        )
    }
    
    private func getCurrentValue() -> Double {
        if let quantity = Double(quantityText) {
            return quantity * (selectedCoin?.currentPrice ?? 0)
        }
        return 0
    }
    
    private func saveButtonPressed() {
        guard let coin = selectedCoin else {return}
         // save to portfolio
        
        withAnimation(.easeIn) {
            removeSelectEffect()
        }
        
        // hide keyboard
        UIApplication.shared.endEditing()
    }
    
    private func removeSelectEffect() {
        selectedCoin = nil
        viewModel.searchText = ""
    }
}

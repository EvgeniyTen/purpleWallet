//
//  CoinLogoView.swift
//  PurpleWallet
//
//  Created by user on 11.12.2022.
//

import SwiftUI

struct CoinLogoView: View {
    
    let coin: CoinModel
    
    var body: some View {
        VStack {
            CoinImageView(coin: coin)
                .frame(width: 35, height: 35)
                .padding(.top)

            Text(coin.symbol.uppercased())
                .font(.callout)
                .bold()
                .foregroundColor(.theme.accent)
                .lineLimit(1)
                .minimumScaleFactor(0.5)
//            Text(coin.name.uppercased())
//                .font(.footnote)
//                .foregroundColor(.theme.secondaryText)
//                .lineLimit(2)
//                .minimumScaleFactor(0.5)
//                .multilineTextAlignment(.center)
                .padding(.bottom)
        }
        .frame(width: 75)
    }
}

struct CoinLogoView_Previews: PreviewProvider {
    static var previews: some View {
        CoinLogoView(coin: mock.coin)
    }
}

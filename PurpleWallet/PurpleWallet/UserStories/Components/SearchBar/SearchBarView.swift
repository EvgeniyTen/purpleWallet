//
//  SearchBarView.swift
//  PurpleWallet
//
//  Created by user on 27.11.2022.
//

import SwiftUI

struct SearchBarView: View {
    
    @Binding var searchText: String
    var body: some View {
        HStack {
            Image(systemName: "magnifyingglass.circle")
                .foregroundColor(
                    searchText.isEmpty ? .theme.secondaryText : .theme.accent
                )
            
            TextField("Secrch by name or symbol...", text: $searchText)
                .foregroundColor(.theme.accent)
                .overlay(
                    Image(systemName: "xmark.circle")
                        .padding()
                        .offset(x: 5)
                        .foregroundColor(.theme.accent)
                        .opacity(searchText.isEmpty ? 0.0 : 1.0 )
                        .onTapGesture {
                            searchText = ""
                            UIApplication.shared.endEditing()
                        }
                    ,alignment: .trailing
                )
                .autocorrectionDisabled(true)
        }
        .font(.footnote)
        .padding()
        .background(
        RoundedRectangle(cornerRadius: 15)
            .fill(Color.theme.background).opacity(0.15)
            .shadow(color: .black, radius: 10)
                 )
        .padding()
    }
}

struct SearchBarView_Previews: PreviewProvider {
    static var previews: some View {
        SearchBarView(searchText: .constant(""))
    }
}

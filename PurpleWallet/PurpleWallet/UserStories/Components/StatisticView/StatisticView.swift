//
//  StatisticView.swift
//  PurpleWallet
//
//  Created by user on 27.11.2022.
//

import SwiftUI

struct StatisticView: View {
    
    let state: StatisticModel
    
    var body: some View {
        VStack(alignment: .leading, spacing: 4){
            Text(state.title)
                .font(.caption)
                .foregroundColor(.theme.secondaryText)
            Text(state.value)
                .font(.headline)
                .foregroundColor(.theme.accent)
            
            HStack(spacing: 4) {
                Image(systemName: "triangle.fill")
                    .font(.caption2)
                    .rotationEffect(Angle(degrees: (state.percengaheChanged ?? 0) >= 0 ? 0 : 180))
                Text(state.percengaheChanged?.asPercentString() ?? "")
                    .font(.caption)
                    .bold()
            }
            .foregroundColor((state.percengaheChanged ?? 0) >= 0 ? .theme.green : .theme.red)
            .opacity(state.percengaheChanged == nil ? 0.0 : 1.0)
        }
    }
}

struct StatisticView_Previews: PreviewProvider {
    static var previews: some View {
        StatisticView(state: mock.mockState)
    }
}

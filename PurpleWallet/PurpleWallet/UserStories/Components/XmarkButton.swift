//
//  XmarkButton.swift
//  PurpleWallet
//
//  Created by user on 10.12.2022.
//

import SwiftUI

struct XmarkButton: View {
    
    var dismiss: DismissAction


    var body: some View {
        Button(action: {
            dismiss()
        }, label: {
            Image(systemName: "xmark")
                .font(.footnote)
        })
        
    }
}

struct XmarkButton_Previews: PreviewProvider {
    static var previews: some View {
        XmarkButton(dismiss: mock.dismiss)
    }
}

//
//  LocalFileManager.swift
//  PurpleWallet
//
//  Created by user on 25.11.2022.
//

import Foundation
import SwiftUI

class LocalFileManager {

    static let instance = LocalFileManager()
    private init() {}
    
    func saveImage(image: UIImage, imageName: String, folderName: String) {
        //create folder
        createFolderIfNeeded(folderName: folderName)
        
        //get path for image
        guard let data = image.jpegData(compressionQuality: 100),
              let url = getURLForImage(imageName: imageName, folderName: folderName)
        else { return }
        
        //save image to path
        do {
            try data.write(to: url)
        } catch let error {
            print("Error saving image. \(imageName) \(error)")
        }
    }
    
    func getImage(imageName: String, folderName: String) -> UIImage? {
        if let url = getURLForImage(imageName: imageName, folderName: folderName), FileManager.default.fileExists(atPath: url.path()) {
            return UIImage(contentsOfFile: url.path())
        } else {
            print("Error getting url for image. \(imageName)")
            return nil
        }
    }
    
    private func createFolderIfNeeded(folderName: String) {
        guard let url = getURLForFolder(folderName: folderName) else { return }
        if !FileManager.default.fileExists(atPath: url.path) {
            do {
                try FileManager.default.createDirectory(at: url, withIntermediateDirectories: true)
            } catch let error {
                print("Error creating directory. \(folderName) \(error)")
            }
        }
    }
    
    private func getURLForFolder(folderName: String) -> URL? {
        if let url = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask).first {
            return url.appendingPathComponent(folderName)
        } else {
            print("Error creating url for \(folderName)")
            return nil
        }
    }
    
    private func getURLForImage(imageName: String, folderName: String) -> URL? {
        guard let folderURL = getURLForFolder(folderName: folderName) else { return nil }
        return folderURL.appendingPathComponent(imageName + ".jpeg")
    }
}
